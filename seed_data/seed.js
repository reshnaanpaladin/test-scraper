const fs = require("fs");
const path = require("path");
const envPath = path.join(__dirname, "../.env");

console.log(`Env path scrape.js --> ${envPath}`);
require("dotenv").config({ path: envPath });
// const fs = require("fs")
const marketService = require("../services/models/market.service");

const jsonFile = `${process.env.MARKET}_audible_with_catname.json`;

// const records =  JSON.parse(fs.readFileSync("new_main_cat.json",'utf8'))
const records = JSON.parse(fs.readFileSync(jsonFile, { encoding: "utf-8" }));

function chunkArray(list, size) {
  const out = [];
  for (let i = 0; i < list.length; i += size) {
    out.push(list.slice(i, i + size));
  }
  return out;
}

(async () => {
  const insertChunks = chunkArray(records, 100);

  const failedIndex = [];

  for (let index = 0; index < insertChunks.length; index++) {
    const recordList = insertChunks[index];

    try {
      const result = await marketService.bulkAddOrUpdate(recordList);
      if (result.success) {
        console.log(`Done with ${index + 1}/${insertChunks.length}`);
      } else {
        console.log(`Failed on ${index + 1}`);
        failedIndex.push(index);
      }
    } catch (error) {
      console.log(`Error ===>`, error);
    }
  }
  fs.writeFileSync("failed_index.txt", JSON.stringify(failedIndex, null, 2));
  process.exit();
})();
