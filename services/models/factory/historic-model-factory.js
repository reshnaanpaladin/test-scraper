// get the database from process.evn and return a sequelize entity

const HistoricModelFactory = (tableName) => {
  return function (sequelize, DataTypes) {
    const HistoricData = sequelize.define(
      "HistoricData",
      {
        id: {
          autoIncrement: true,
          type: DataTypes.INTEGER(11),
          allowNull: false,
          primaryKey: true,
        },
        cat_id: {
          type: DataTypes.INTEGER(11),
          allowNull: false,
        },
        scrapedAt: {
          type: DataTypes.STRING(250),
          allowNull: true,
        },
        url: {
          type: DataTypes.STRING(250),
          allowNull: true,
        },
        title: {
          type: DataTypes.STRING(250),
          allowNull: true,
        },
        topSellerRank: {
          type: DataTypes.STRING(250),
          allowNull: true,
        },
        author: {
          type: DataTypes.STRING(250),
          allowNull: true,
        },
        type: {
          type: DataTypes.STRING(250),
          allowNull: true,
        },
        price: {
          type: DataTypes.FLOAT,
          allowNull: true,
        },
        numReviews: {
          type: DataTypes.FLOAT,
          allowNull: true,
        },
        rating: {
          type: DataTypes.FLOAT,
          allowNull: true,
        },
        absr: {
          type: DataTypes.INTEGER(11),
          allowNull: true,
        },
      },
      {
        sequelize,
        tableName: tableName,
        timestamps: true,
        createdAt: "created_on",
        updatedAt: false,
      }
    );

    return HistoricData;
  };
};

module.exports = HistoricModelFactory;
