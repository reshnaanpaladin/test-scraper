// const ModelService = require('./services/models/model-service');
// const model = require('./database/models/entity');
const marketService = require("./services/models/market.service");
const utilService = require("./helpers/util.service");
const httpScrapper = require("./modules/http-scrapper");

const moment = require("moment");

const fs = require("fs");
const path = require("path");
const historicDataService = require("./services/models/historic-data-service");
const { logger } = require("./services/logger/loggerService");
const {
  notification,
} = require("./services/notifications/notification-service");

// const httpClient = require('./helpers/http-client');

const envPath = path.join(__dirname, ".env");

console.log(`Env path scrape.js --> ${envPath}`);

require("dotenv").config({ path: envPath });

// function get records  greater than day

async function getMarketCollections() {
  const DAY_LIMIT = process.env.DAY_LIMIT;

  console.log(`DAY_LIMIT --.`, DAY_LIMIT);
  const limit = moment().subtract(DAY_LIMIT, "days").toDate();

  console.log(`startDate -->`, limit);

  const collections = await marketService.getCollection(limit);
  // console.log(`collections --< func`,collections);
  return collections;
}

async function main() {
  console.log(`Started Main`);

  try {
    // get collections

    // (`xx- ->`,x);
    const market = process.env.MARKET;
    const tableName = process.env.TABLE_NAME;
    const collections = await getMarketCollections();
    // console.log(`collections -->`,collections);
    if (!collections.success) {
      throw new Error("Failed to get to collections");
    }

    let { data } = collections;

    if (data.length <= 0) {
      console.log(`No records to update`);
      logger.warn(`No records to update for ${tableName}`);
      return;
    }

    // chunck collections

    const records = data.map((x) => x.toJSON());

    console.log(`records -->`, records.length);

    const recordChunks = utilService.chunkArray(records, 100);

    console.log(`recordChunks -->`, recordChunks.length);

    let failedRequests = [];
    const totalFailedRequest = [];
    let updateResponse = [];
    const updateFields = [
      "one",
      "one_num_reviews",
      "one_avg_rating",
      "ten",
      "ten_num_reviews",
      "ten_avg_rating",
      "updated_on",
    ];

    for (let index = 0; index < recordChunks.length; index++) {
      const chunck = recordChunks[index];

      for (let i = 0; i < chunck.length; i++) {
        const item = chunck[i];

        console.log(`item.amazonUrl -->`, item.amazonUrl);
        const response = await httpScrapper.amazonScraper.scrape({
          url: item.amazonUrl,
          market,
        });

        if (response == null) {
          totalFailedRequest.push(item);
          delete item["updated_on"];
          failedRequests.push(item);
          utilService.sendAndLogGeneralInformation(
            `Failed to get absr value for ${item.amazonUrl}`
          );
        } else {
          console.log(`Done ID -->`, item.id);
          const updateItem = { ...item, ...response };
          delete updateItem["updated_on"];
          delete updateItem["created_on"];
          updateResponse.push(updateItem);
        }

        if (updateResponse.length > 0) {
          const updateStatus = await marketService.bulkAddOrUpdate(
            updateResponse,
            updateFields
          );
          if (updateStatus.success) {
            const historicData = updateResponse.map((x) => {
              const item = JSON.parse(JSON.stringify(x));
              const id = item["id"];
              delete item["id"];
              delete item["created_on"];
              item["cat_id"] = id;
              return item;
            });
            const historicDataUpdate =
              await historicDataService.bulkAddOrUpdate(historicData);
          }

          updateResponse = [];
        } else {
          console.log(`Some updates failed :(`);
          const failedUpdate = await marketService.bulkAddOrUpdate(
            failedRequests,
            ["updated_on"]
          );
          console.log(`Failed Update Status -->`, failedUpdate);
          console.log(`Updated Failed once as completed`);
          failedRequests = [];
        }
        console.log(`Started Sleeping`);
        await utilService.randomSleep();
        console.log(`Done sleeping`);
      }
    }

    console.log(`Failed Response -->`, totalFailedRequest.length);
    utilService.sendAndLogGeneralInformation(
      `Failed Response for ${tableName} - ${totalFailedRequest}`
    );
    const currentDate = new Date();
    fs.writeFileSync(
      `${market}_${tableName}_failed_${totalFailedRequest}_${currentDate.toISOString()}.txt`,
      JSON.stringify(totalFailedRequest)
    );

    console.log(`Done with the update----`);
  } catch (error) {
    console.log("error==>",error);
    console.log(`Done with error --> ${error.message}`);
    logger.error(`Failed to scrape category -${error.message}`);
  }
}

(async () => {
  setTimeout(async () => {
    const response = await httpScrapper.amazonScraper.scrape({
      url: "https://www.amazon.de/gp/bestsellers/digital-text/6692009031/ref=zg_bs_nav_kinc_4_6692008031",
      market: "de",
      // followNextPage:true
    });

    console.log(`response `, response);

    process.exit();
  }, 5000);
})();
