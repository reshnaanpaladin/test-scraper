const ModelService = require("./model-service");
const Op = require("sequelize").Op;
const Market =  require("../../database/models").Market


class MarketService extends ModelService{


    constructor(model){
        super(model)
    }

    async getCollection(limit){
        console.log(`date -->`,limit);
        // console.log();
        const clause  = {
            updated_on : {
                [Op.lte] : limit
            }
        }
        console.log(`updated -->`,clause);
        return await this.findAllWhere(clause)
    }

}


const marketService = new MarketService(Market)

module.exports =  marketService
