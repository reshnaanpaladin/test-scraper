const serviceResponse = function (success = false, data = null, msg = "") {
    return { success, msg, data }
}

const failedService = function (data = null, msg = "") {

    return serviceResponse(false, data, msg)
}

const successService = function (data = null, msg = "") {

    return serviceResponse(true, data, msg)
}

const serviceHelper = {
    serviceResponse,
    failedService,
    successService,
}

module.exports = serviceHelper