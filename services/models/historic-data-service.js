const ModelService = require("./model-service");
const HistoricData = require("../../database/models").HistoricData


class HistoricDataService extends ModelService{

    constructor(){
        super(HistoricData)
    }

}

const historicDataService =  new HistoricDataService()

module.exports = historicDataService