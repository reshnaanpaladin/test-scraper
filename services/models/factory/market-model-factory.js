// get the database from process.evn and return a sequelize entity


const MarketModelFactory = (tableName) => {

    return function(sequelize, DataTypes) {
        const Market = sequelize.define('Market', {
          id: {
            autoIncrement: true,
            type: DataTypes.INTEGER(11),
            allowNull: false,
            primaryKey: true
          },
          catName: {
            type: DataTypes.STRING(250),
            allowNull: false
          },
          catString: {
            type: DataTypes.STRING(250),
            allowNull: false
          },
          amazonUrl: {
            type: DataTypes.STRING(250),
            allowNull: false
          },
          one: {
            type: DataTypes.INTEGER(11),
            allowNull: true
          },
          ten: {
            type: DataTypes.INTEGER(11),
            allowNull: true
          },
          one_num_reviews: {
            type: DataTypes.INTEGER(11),
            allowNull: true
          },
          ten_num_reviews: {
            type: DataTypes.INTEGER(11),
            allowNull: true
          },
          one_avg_rating: {
            type: DataTypes.FLOAT,
            allowNull: true
          },
          ten_avg_rating: {
            type: DataTypes.FLOAT,
            allowNull: true
          },
          isRetired: {
            type: DataTypes.INTEGER(1),
            allowNull: true
          },
          skipCount: {
            type: DataTypes.INTEGER(11),
            allowNull: true
          },
          
        }, {
          sequelize,
          tableName: tableName,
          timestamps: true,
          createdAt: 'created_on',
          updatedAt: 'updated_on',
        });

        return Market
      };
 
}




module.exports = MarketModelFactory
