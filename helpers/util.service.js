const { logger } = require("../services/logger/loggerService");
const {
  notification,
} = require("../services/notifications/notification-service");

function chunkArray(list, size) {
  const out = [];
  for (let i = 0; i < list.length; i += size) {
    out.push(list.slice(i, i + size));
  }
  return out;
}

function sleep(ms) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

function getRandomNumBetween(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min; //The maximum is inclusive and the minimum is inclusive
}

function randomSleep() {
  const randomNumer = getRandomNumBetween(1, 10);
  const sleepTime = randomNumer * 1000;
  console.log(`Sleeping for ${sleepTime}`);
  return sleep(sleepTime);
}

async function sendAndLogGeneralInformation(message = "") {
  await notification.generalInformationNotifation({ message });
  logger.info(message);
}

module.exports = {
  chunkArray,
  sleep,
  randomSleep,
  sendAndLogGeneralInformation,
};
