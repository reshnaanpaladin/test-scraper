const path  =  require("path")

const envPath  =  path.join(__dirname, "../../.env")
console.log(`Env path entity.js --> ${envPath}`);
require('dotenv').config({path:envPath});
const MarketModelFactory = require("../../services/models/factory/market-model-factory");
const model =  MarketModelFactory(process.env.TABLE_NAME)

console.log(`process.env.TABLE_NAME -->`,process.env.TABLE_NAME);
// console.log(`model -->`, model);

module.exports = model