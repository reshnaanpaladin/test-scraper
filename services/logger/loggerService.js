require("dotenv").config();
const pinoCloudwatch = require("pino-cloudwatch");

const pino = require("pino");

const { TABLE_NAME, AWS_ACCESS_KEY, AWS_SECRET_KEY } = process.env;

console.log(AWS_ACCESS_KEY, AWS_SECRET_KEY);

const logger = pinoCloudwatch({
  group: `rocket-category-scraper_${new Date().toJSON().split("T")[0]}_${TABLE_NAME}`,
  prefix: "rocket-category-scraper",
  aws_region: "us-east-2",
  aws_access_key_id: AWS_ACCESS_KEY,
  aws_secret_access_key: AWS_SECRET_KEY,
  interval: 5000,
});


const pinoLogger = pino(logger);

module.exports.logger = pinoLogger;
