// const ModelService = require('./services/models/model-service');
// const model = require('./database/models/entity');
const marketService = require("./services/models/market.service");
const utilService = require("./helpers/util.service");
const httpScrapper = require("./modules/http-scrapper");

const moment = require("moment");

const fs = require("fs-extra");
const path = require("path");
const historicDataService = require("./services/models/historic-data-service");
const { logger } = require("./services/logger/loggerService");
const {
  notification,
} = require("./services/notifications/notification-service");
// const fs = require("fs");
// const httpClient = require('./helpers/http-client');

const envPath = path.join(__dirname, ".env");

console.log(`Env path scrape.js --> ${envPath}`);

require("dotenv").config({ path: envPath });

// function get records  greater than day

async function getMarketCollections() {
  const DAY_LIMIT = process.env.DAY_LIMIT;

  console.log(`DAY_LIMIT --.`, DAY_LIMIT);
  const limit = moment().subtract(DAY_LIMIT, "days").toDate();

  console.log(`startDate -->`, limit);

  const collections = await marketService.getCollection(limit);
  // console.log(` getCollection collections --< func`,collections);
  return collections;
}

async function main() {
  console.log(`Started Main`);

  try {
    // get collections

    // console.log(`xx- ->`,x);
    const market = process.env.MARKET;
    const tableName = process.env.TABLE_NAME;
    let collections = await getMarketCollections();
    // console.log(`getMarketCollections -->`, collections);
    if (!collections.success) {
      throw new Error("Failed to get to collections");
    }

    let { data } = collections;

    if (data.length <= 0) {
      console.log(`No records to update`);
      logger.warn(`No records to update for ${tableName}`);
      return;
    }

    // chunck collections

    let records = data.map((x) => x.toJSON());

    console.log(`records -->`, records.length);

    // records = records.filter((x, i) => i < 1);

    const recordChunks = utilService.chunkArray(records, 10);

    console.log(`recordChunks -->`, recordChunks.length);

    let failedRequests = [];
    const totalFailedRequest = [];
    let updateResponse = [];
    const updateFields = [
      "one",
      "one_num_reviews",
      "one_avg_rating",
      "ten",
      "ten_num_reviews",
      "ten_avg_rating",
      "isRetired",
      "skipCount",
      "updated_on"
    ];

    for (let index = 0; index < recordChunks.length; index++) {
      const chunck = recordChunks[index];

      for (let i = 0; i < chunck.length; i++) {
        const item = chunck[i];

        // console.log(`items -->`, item);

        //check if retire
        //if retired catogory exist stop scraping
        //else then scrape 
        let response;
        if (item.isRetired == 1) {
          response = null;
          console.log(`Found a retired catagory in db ==> ${item.amazonUrl}`);
        } else {
        // console.log(`item.amazonUrl -->`, item.amazonUrl);
          response = await httpScrapper.amazonScraper.scrape({
            url: item.amazonUrl,
            market,
            // followNextPage: true,
            id: item.id,
          });
        }
        const { category, historic, catId } = response || {};




        // process.exit()

        console.log("format category", JSON.stringify(category, null, 2));
        // console.log('format historic', historic[0]);
        // console.log('historic 113 ====>', JSON.stringify(historic));

        if (category == null) {
          totalFailedRequest.push(item);
          delete item["updated_on"];
          failedRequests.push(item);
          utilService.sendAndLogGeneralInformation(
            `Failed to get absr value for ${item.amazonUrl}`
          );
        } else {
          //get rank one and rank ten in db
          let firstRank = historic.filter((x) => x.topSellerRank == "1");
          let tenthRank = historic.filter((x) => x.topSellerRank == "10");
          const oneAbsr = firstRank[0].absr;
          const tenthAbsr = tenthRank[0].absr;
          let skipCount = item.skipCount;
          let skipLimit = 3;

          // check absr is null then increase skipped count by  one 
          if (oneAbsr == null || tenthAbsr == null) {
            if (skipCount <= skipLimit) {
              skipCount += 1;
            } else {
              item.isRetired = 1;
              // skipCount = 0;
            }
          } else {
            skipCount = 0;
          }


          console.log(`Done ID -->`, item.id);
          const updateItem = { ...item, ...category, skipCount };

          console.log("updated Item===>", JSON.stringify(updateItem, null, 2));
          delete updateItem["updated_on"];
          delete updateItem["created_on"];
          updateResponse.push(updateItem);
        }

        if (updateResponse.length > 0) {
          const updateStatus = await marketService.bulkAddOrUpdate(
            updateResponse,
            updateFields
          );
          if (updateStatus.success && historic) {
            let isOne = item.one;
            let isTen = item.ten;
            console.log("items ====>", item);
            const historicData = historic.map((x) => {
              const item = JSON.parse(JSON.stringify(x));
              // console.log("absr line 151 ===>",item["absr"])
              // fs.writeFileSync(`absr/absr${item["absr"]}.json`, JSON.stringify(item));
              //check if absr is null 
              //if absr value is null then add old absr value
              if (item["absr"] == null) {
                //one 
                if (item["topSellerRank"] == 1) {
                  //add old absr value
                  item["absr"] = isOne;
                  fs.writeFileSync(`replaceValueABSR/absrOne${isOne}.json`, JSON.stringify(item));
                }
                //ten
                if (item["topSellerRank"] == 10) {
                  //add old absr value
                  item["absr"] = isTen;
                  fs.writeFileSync(`replaceValueABSR/absrTen${isTen}.json`, JSON.stringify(item));
                }
              }
              item["cat_id"] = catId;
              return item;
            });

            await historicDataService.bulkAddOrUpdate(historicData);
          }

          updateResponse = [];
        } else {
          console.log(`Some updates failed :(`);
          const failedUpdate = await marketService.bulkAddOrUpdate(
            failedRequests,
            ["updated_on"]
          );
          console.log(`Failed Update Status -->`, failedUpdate);
          console.log(`Updated Failed once as completed`);
          failedRequests = [];
        }
        console.log(`Started Sleeping`);
        await utilService.randomSleep();
        console.log(`Done sleeping`);
        break;
      }
    }

    console.log(`Failed Response -->`, totalFailedRequest.length);
    utilService.sendAndLogGeneralInformation(
      `Failed Response for ${tableName} - ${totalFailedRequest}`
    );
    const currentDate = new Date();

    const logPath = `${tableName}_failed_${totalFailedRequest}_${currentDate.toISOString().split("T")[0]
      }.txt`;

    await fs.ensureFile(logPath);

    fs.writeFileSync(logPath, JSON.stringify(totalFailedRequest));

    console.log(`Done with the update----`);
  } catch (error) {
    console.log(error);
    console.log(`Done with error --> ${error.message}`);
    logger.error(`Failed to scrape category -${error.message}`);
  }
}

(async () => {
  setTimeout(async () => {
    const market = process.env.MARKET;
    const tableName = process.env.TABLE_NAME;

    utilService.sendAndLogGeneralInformation(
      `Started scraper for ${market}. Updating ${tableName}`
    );

    await main();
    // console.log(httpScrapper.amazonScraper.helper.cleanNumberString("40,17 â‚¬"))
    // console.log(httpScrapper.amazonScraper.helper.cleanNumberString("2,210,935"))

    utilService.sendAndLogGeneralInformation(
      `Completed scraper for ${market}. Updating ${tableName}`
    );

    process.exit();
  }, 5000);
})();