const fs = require("fs");
const path = require("path");
const rdsCa = fs.readFileSync(__dirname + "/rds-combined-ca-bundle.pem");

const envPath = path.join(__dirname, "../../.env");

console.log(`Env path Config.js --> ${envPath}`);

require("dotenv").config({ path: envPath }); // this is important!

const dbConfig = {
  production: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: "mariadb",
    port: process.env.DB_PORT,
    dialectOptions: {
      ssl: {
        rejectUnauthorized: true,
        ca: [rdsCa],
      },
    },
    pool: {
      max: 5,
      min: 1,
      idle: 10000,
      acquire: 10000,
      evict: 10000,
      handleDisconnects: true,
    },
  },
  development: {
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    dialect: "mariadb",
    pool: {
      max: 5,
      min: 1,
      idle: 10000,
      acquire: 10000,
      evict: 10000,
      handleDisconnects: true,
    },
  },
};

if (process.env.IS_HEROKU) {
  delete dbConfig["production"]["dialectOptions"];
}

module.exports = dbConfig;
