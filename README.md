## SQL Schema

// Historical Category

CREATE TABLE `de_ebook_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);
CREATE TABLE `us_ebook_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);

CREATE TABLE `uk_ebook_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);

CREATE TABLE `de_book_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);

CREATE TABLE `us_book_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);

CREATE TABLE `uk_book_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);

CREATE TABLE `uk_audible_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);

CREATE TABLE `us_audible_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);

CREATE TABLE `de_audible_historical`(
 `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
  `cat_id` int(11) DEFAULT NULL,
   `scrapedAt`     varchar(250) NOT NULL 
  ,`url`           varchar(250) NOT NULL
  ,`title`         varchar(250) NOT NULL
  ,`topSellerRank` int(11)  NOT NULL
  ,`author`        varchar(250) NOT NULL
  ,`type`          varchar(250) NOT NULL
  ,`price`         float  NOT NULL
  ,`numReviews`    float  NOT NULL
  ,`rating`        float  NOT NULL
  ,`absr`          int(11)  NOT NULL,
    `created_on` timestamp NOT NULL DEFAULT current_timestamp()
);


## Change data query

UPDATE `acs`.`de_book` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`us_book` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`uk_book` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`uk_ebook` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`us_ebook` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`de_ebook` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`de_audible` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`us_audible` SET `updated_on`='2021-07-10 11:48:07';
UPDATE `acs`.`uk_audible` SET `updated_on`='2021-07-10 11:48:07';


DROP TABLE `acs`.`de_audible_historical`;
DROP TABLE `acs`.`uk_audible_historical`;
DROP TABLE `acs`.`us_audible_historical`;

DROP TABLE `acs`.`us_ebook_historical`;
DROP TABLE `acs`.`uk_ebook_historical`;
DROP TABLE `acs`.`de_ebook_historical`;

DROP TABLE `acs`.`us_book_historical`;
DROP TABLE `acs`.`uk_book_historical`;
DROP TABLE `acs`.`de_book_historical`;
