const config = require("../configs/config");
const { absrConfig } = config;
const fs = require("fs");

const countOccurances = (str, finder) => {
  return str.split("").filter((x) => x == finder);
};

const cleanNumberString = (str) => {
  if (!str) {
    return str;
  }

  str = str.replace(/[^0-9\.,-]+/g, "");

  str = str.replace(/,/g, "|");

  // change "," decimal separator for German
  if (str.length >= 4) {
    if (str[str.length - 3] == "|") {
      // str[str.length - 3] = ".";
      const strClone = str.split("");
      strClone[str.length - 3] = ".";
      str = strClone.join("");
      console.log(`str[str.length - 3] ===>`, str);
    }
  }

  str = str.replace(/[^0-9\.-]+/g, "");

  return str;
};

function cleanAmazonUrl(url) {
  const splits = url.split("/ref=");

  // better to have a regex check for a good amazon url

  return splits[0] ? splits[0] : url;
}
// filter the absr value 
const parseABR = (htmlDoc) => {
  // console.log("htmlDoc in parseABSR===>",htmlDoc);
  // fs.writeFileSync(`items/parseABRhtmlDoc${parseInt(Math.random() * 10)}.json`, JSON.stringify(htmlDoc));
  
  let allText = Array.from(htmlDoc.querySelectorAll("*")).map((x) =>
    x.textContent ? x.textContent.trim().replace(/\n/g, "").toLowerCase() : ""
  );
  const absrRegex = new RegExp(absrConfig.regex, "igm");

  let filteredText = allText.filter(
    (x) => x.includes(absrConfig.finderString) && x.match(absrRegex)
  );
  console.log(`filteredText --> `, filteredText);
  console.log(`filteredText --> `, filteredText.length);

  if (filteredText.length < 0) {
    return null;
  } else if (filteredText.length > 1) {
    filteredText = filteredText.filter(
      (x) =>
        x.includes(absrConfig.finderString) &&
        x.includes(absrConfig.finderStringAffirm) &&
        x.match(absrRegex)
    );
    console.log(`double filter -->`, filteredText);
    if (filteredText.length > 1) {
      // second set of filteration
      const pointer = JSON.parse(absrConfig.finderStringPointers);
      filteredText = filteredText.filter(
        (x) => pointer.some((y) => x.includes(y)) && x.match(absrRegex)
      );
    }
  }

  //3995

  const abrsString = filteredText[0];
  return extractABSR(abrsString);
};

const extractABSR = (abrsString) => {
  const absrRegex = new RegExp(absrConfig.regex, "igm");
  let absr = null;

  while ((matches = absrRegex.exec(abrsString)) !== null) {
    // This is necessary to avoid infinite loops with zero-width matches
    if (matches.index === absrRegex.lastIndex) {
      absrRegex.lastIndex++;
    }

    const outlier = [
      "{",
      "}",
      "(",
      ")",
      "margin",
      "padding",
      "list",
      "display",
    ];

    const hasOutliers = outlier.some((x) => matches[0].includes(x));

    if (hasOutliers) {
      absr = null;
    } else {
      console.log(`Matches  -->`, matches);
      const matchGroups = JSON.parse(JSON.stringify(matches.groups));
      if (matchGroups) {
        absr = matchGroups["rank"] ? matchGroups["rank"] : null;
      }
    }
  }
  console.log("absr line 111 ====>", absr)
  return absr;
};
//this function is use to get 50 records in amazon page 
const getTop50Results = (htmlDoc) => {
  try {
    const products = [];
    // let nextLink = null;
    const itemSpans = htmlDoc.querySelectorAll(config.itemSelectorMain);
    let pageTitle = htmlDoc.querySelector(config.mainPageTitle);
    // if (getNextLink) {
    //   const aTag = htmlDoc.querySelector(config.nextPageLinkSector);
    //   if (aTag) {
    //     nextLink = aTag.href;
    //   }
    // }
    // console.log("getTop50Results html doc ===>", htmlDoc);
    for (let item of itemSpans) {
      let tmp = {
        scrapedAt: new Date(),
        url: item.querySelector(config.urlSelector)
          ? item.querySelector(config.urlSelector).getAttribute("href")
          : "",
        title: (item.querySelector(config.titleSelector2)
          ? item.querySelector(config.titleSelector2).textContent
          : ""
        ).trim().replace("/[0-9a-zA-Z]+/g", ""),
        topSellerRank: (item.querySelector(config.topSellerRankSelector)
          ? item.querySelector(config.topSellerRankSelector).textContent
          : ""
        ).replace(/#/gi, ""),
        PageTitle: pageTitle ? pageTitle.textContent : "",
        author: item.querySelector(config.authorSelector)
          ? item.querySelector(config.authorSelector).textContent
          : "",
        price: item.querySelector(config.priceSelector2)
          ? item.querySelector(config.priceSelector2).textContent : "",
        numReviews: (item.querySelector(config.numReviewsSelector)
          ? item.querySelector(config.numReviewsSelector).textContent
          : ""
        ).replace(/,/, ""),
        rating: (item.querySelector(config.ratingSelector)
          ? item.querySelector(config.ratingSelector).title
          : ""
        )
          .replace(/ out of 5 stars/gi, "")
          .replace(/ von 5 Sternen/gi, "")
          .replace(/,/gi, "."),
      };

      if (tmp["numReviews"].length <= 0 || tmp["rating"].length <= 0) {
        tmp["type"] = item.querySelector(config.bookTypeSelectorNoReview)
          ? item.querySelector(config.bookTypeSelectorNoReview).textContent
          : "";
      } else {
        tmp["type"] = item.querySelector(config.bookType)
          ? item.querySelector(config.bookType).textContent
          : "";
      }

      products.push(tmp);
    }

    // console.log("products ===>", products);

    return {
      products,
      // nextLink: nextLink,
    };
  } catch (error) {
    console.log(error);
    return null;
  }
};

module.exports = {
  parseABR,
  getTop50Results,
  cleanNumberString,
  cleanAmazonUrl,
};
