const marketLinks = {
  us: "https://www.amazon.com",
  de: "https://www.amazon.de",
  uk: "https://www.amazon.co.uk",
};

module.exports = {
  itemSelector: "#gridItemRoot",
  itemSelectorMain: "#gridItemRoot",
  mainPageTitle:'._p13n-zg-list-grid-desktop_style_card-title__2sYgw:nth-child(1)',
  titleSelector: ".p13n-sc-truncated",
  titleSelector2: 'div[class="_p13n-zg-list-grid-desktop_truncationStyles_p13n-sc-css-line-clamp-1__1Fn1y"]',
  topSellerRankSelector: ".zg-bdg-ctr:nth-child(1)",
  authorSelector: '.a-size-small:nth-child(1)',
  bookTypeSelector: ".a-row:nth-child(4) span.a-color-secondary",
  bookTypeSelectorNoReview: ".a-row:nth-child(3) span.a-color-secondary",
  bookType:".a-size-small:nth-child(1)",
  priceSelector: ".p13n-sc-price",
  priceSelector2:'span[class="_p13n-zg-list-grid-desktop_price_p13n-sc-price__3mJ9Z"]',
  numReviewsSelector: ".a-icon-row a:nth-child(1) .a-size-small",
  ratingSelector: ".a-icon-row a:nth-child(1)",
  urlSelector: "a:nth-child(1)",
  absrSelector1: ".a-keyvalue tr:last-child span:first-child",
  absrSelector2: "#SalesRank",
  absrSelector3: "#productDetails_detailBullets_sections1 tr:nth-child(3) td",
  absrConfig: {
    finderStringAffirm: "sell",
    finderString: "best",
    finderStringPointers: '["amazon","rank","rang"]',
    regex:
      "^(Amazon|Best)[\\s\\w+-]?[:?[^a-zA-Z0-9]+[^0-9]+(?<rank>[-+]?(?:[0-9]+,)*[0-9]+(?:\\.[0-9]+)?)",
  },
  nextPageLinkSector: "ul.a-pagination > li.a-last > a",
  marketLinks,
};
