const JSDOM = require("jsdom").JSDOM;
const scraperapiClient = require('scraperapi-sdk')('1b09db0761257e6d9d1cc4679d93eb09');
const axios = require("axios").default;

console.log(`PROXY --->`, {
  // Proxy settings
  host: process.env.PROXY_HOST, // Defaults to 'localhost'
  port: process.env.PROXY_PORT, // Defaults to 80
  // Basic authorization for proxy server if necessary
  proxyAuth: process.env.PROXY_AUTH,
});

const getAxios = async (url) => {
  try {

    console.log("http client url ====>", url)
    const { FALLBACK_LAMBDA } = process.env;
    const fallbackUrl = `${FALLBACK_LAMBDA}${url}`;

    console.log(`Using Fallback url ===>`, fallbackUrl);

    const response = await axios.get(fallbackUrl, {
      headers: {
        "Content-Type": getUserAgent(),
      },
    });
    const body = response.data;

    if (body == null || response.statusCode != 200) {
      return null;
    } else {
      return new JSDOM(body).window.document;
    }
  } catch (error) {
    console.log(error);
    return null;
  }
};


async function Scrapeget(url) {
  try {
    const scrape = await scraperapiClient.get(url);
    return scrape;
  } catch (error) {
    console.log(error)
  }


}




const getUserAgent = () =>
  "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3312.0 Safari/537.36";

const getGot = async (url,
  headers = {},
  isHtml = false
) => {
  console.log("http client url ====>", url);
  console.log("headers ====>", headers);

  let retry = 5;


  while (retry > 0) {
    try {
      //
      body = await Scrapeget(url);
      if (body == null || body == undefined || body == '') {
        retry -= 1;
        console.log(`Retrying after  body error ==> ${url} ==> ${retry}`);
      } else {
        retry = 0;
        console.log(`COmpleted Call with ${url}`);
      }
    } catch (error) {
      console.log(`erroro ==>0`, error)
      retry -= 1;
      console.log(`Retrying after crashing  ==> ${url} ==> ${retry}`);
    }
  }
  if (body == null || body == undefined || body == '') {
    return await getAxios(url);
  } else {
    if (isHtml) {
      return body;
    } else {

      return new JSDOM(body).window.document;
    }

  }
};

const httpClient = {
  get: getGot,
};

module.exports = httpClient;
