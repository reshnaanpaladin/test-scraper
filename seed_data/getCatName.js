const fs = require("fs");

const jsonFilename = "de_audible";
const source = `${jsonFilename}.json`;
const out = `${jsonFilename}_with_catname.json`;
const splitBy = ">";
const splitIndex = 1;

const dump = JSON.parse(fs.readFileSync(source, { encoding: "utf-8" }));

const getCatName = (catString) =>
  catString.split(splitBy).map((x) => x.trim())[splitIndex];

const withCatName = dump.map((item) => ({...item, catName : getCatName(item.catString)}));

fs.writeFileSync(out, JSON.stringify(withCatName, null, 2));
