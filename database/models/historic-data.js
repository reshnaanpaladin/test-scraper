const path  =  require("path")

const envPath  =  path.join(__dirname, "../../.env")
console.log(`Env path entity.js --> ${envPath}`);
require('dotenv').config({path:envPath});
const HistoricModelFactory = require("../../services/models/factory/historic-model-factory");

const table_name =  `${process.env.TABLE_NAME}_historical`
const model =  HistoricModelFactory(table_name)

console.log(`Historic Data DB -->`,table_name);
// console.log(`model -->`, model);

module.exports = model