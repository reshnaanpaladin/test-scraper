const amazonHelper = require("./helpers/amazon-helper");
const httpClient = require("./helpers/http-client");
const { marketLinks } = require("./configs/config");
const cleanDeep = require("clean-deep");
const utilService = require("../../../helpers/util.service");
const { logger } = require("../../../services/logger/loggerService");
const fs = require("fs");

function formatAndCleanNumber(item = {}, keys = []) {
  for (let i = 0; i < keys.length; i++) {
    const keyItem = keys[i];
    if (item[keyItem]) {
      try {

        item[keyItem] = parseFloat(
          amazonHelper.cleanNumberString(item[keyItem])
        );


      } catch (error) {
        console.log("error message ==>", error.message)
      }
    }
    if (item[keyItem] === "" || item[keyItem] === null) {
      item[keyItem] = 0;
      console.log('items--->', item[keyItem]);
    }
  }
  return item;
}
function isNumeric(val) {
  return /^-?\d+$/.test(val);
}
function formatResponse(extractedContent, url) {
  try {
    const response = {};
    let isRetired;
    let isCheckRetired = extractedContent[0].PageTitle;
    // let string = " Best Sellers in 17205"
    let detect = isCheckRetired.split("Best Sellers in")[1];
    //  check type is integer or not isNum()

    console.log("extractedContent Length==>", extractedContent.length)
    if (isCheckRetired == "Best Sellers" || isNumeric(detect) == true) {
      fs.writeFileSync(`retiredFiles/ischeck${parseInt(Math.random() * 10)}.json`, JSON.stringify({ extractedContent, url }));
      isRetired = 1;

    } else {
      isRetired = 0;
    }


    let firstRank = extractedContent.filter((x) => x.topSellerRank == "1");

    if (firstRank.length == 1) {
      const item = firstRank[0];
      response["one"] = item.absr
        ? parseFloat(amazonHelper.cleanNumberString(item.absr))
        : null;
      response["one_num_reviews"] = item.numReviews
        ? parseFloat(amazonHelper.cleanNumberString(item.numReviews))
        : null;
      response["one_avg_rating"] = item.rating ? parseFloat(item.rating) : null;
    }
    console.log("ABSR first Rank --->", firstRank);

    let tenthRank = extractedContent.filter((x) => x.topSellerRank == "10");


    if (tenthRank.length == 1) {
      const item = tenthRank[0];
      response["ten"] = item.absr
        ? parseFloat(amazonHelper.cleanNumberString(item.absr))
        : null;
      response["ten_num_reviews"] = item.numReviews
        ? parseFloat(amazonHelper.cleanNumberString(item.numReviews))
        : null;
      response["ten_avg_rating"] = item.rating ? parseFloat(item.rating) : null;
    }
    console.log("ABSR Tenth Rank --->", tenthRank);

    const { ten, one } = response;

    if (one >= ten) {
      console.log(`ABSR #1 greater than #10 for ${url || ""}`);
      utilService.sendAndLogGeneralInformation(
        `ABSR #1 greater than #10 for ${url || ""}`
      );
      return null;
    }

    let obj = {
      ...response,
      isRetired
    }
    return cleanDeep(obj);
  } catch (error) {
    console.log(`Error in format function  --> ${error.message}`);
    console.log(`extractedContent ==>`, extractedContent);
    return null;
  }
}

async function extractABSRFromResults(resultArray, market, marketUrl) {
  try {
    const httpRequests = resultArray.map((x) => {
      let url = `${marketLinks[market]}${x.url}`;
      url = amazonHelper.cleanAmazonUrl(url);
      console.log(`extractABSRFromResults url -->`, url);
      return httpClient.get(url, { referer: marketUrl });
    });
    logger.info(`Fetching ABSR for ${marketUrl}`);
    const results = await Promise.allSettled(httpRequests);

    results.map((result, index) => {
      if (result.status == "fulfilled") {
        const htmlDoc = result.value;
        const absr = amazonHelper.parseABR(htmlDoc);
        resultArray[index]["absr"] = absr;
      } else {
        logger.info(`Failed in fetching ABSR for ${url}`);
        resultArray[index]["absr"] = null;
      }
    });
    return resultArray;
  } catch (error) {
    console.log(`error ==>`, error);
    return resultArray;
  }
}

async function getPageData(url, cleanUrl = true, market) {
  const items = [];
  let marketUrl = cleanUrl ? amazonHelper.cleanAmazonUrl(url) : url;

  try {
    const checkUrl = new URL(marketUrl)
  } catch (error) {
    const baseLink = marketLinks[market]
    marketUrl = `${baseLink}${marketUrl}`
  }

  const htmlDocument = await httpClient.get(marketUrl);
  if (htmlDocument) {
    const productResults = amazonHelper.getTop50Results(
      htmlDocument,
      cleanUrl
    );

    const { products = [] } = productResults || {};
    console.log(`product length`, productResults.products.length)
    console.log(`market url`, marketUrl)
    items.push(...products);
  } else {
    console.log(`Failed to get html`);
    utilService.sendAndLogGeneralInformation(`Failed to get html for ${url}`);
    return null;
  }
  return items.flat();
}

async function scrape({ url, market, ranks, formatFunc, id }) {
  try {
    const marketUrl = amazonHelper.cleanAmazonUrl(url);
    const productResults = await getPageData(url, true, market);

    if (productResults) {
      if (productResults.length <= 0) {
        console.log(`No products found`);
        utilService.sendAndLogGeneralInformation(
          `No products founds for ${url}`
        );
        return null;
      }

      ranks = ranks || ["1", "10", "100"];


      console.log(`productResults`, productResults.length)
      formatFunc = formatFunc || formatResponse;
      const topSellerRankFilltered = productResults.filter((x) =>
        ranks.includes(x["topSellerRank"])
      );


      const otherRecords = productResults.filter(
        (x) => ranks.includes(x["topSellerRank"]) == false
      );

      console.log(`topSellerRankFilltered --> ${topSellerRankFilltered.length}`, topSellerRankFilltered);

      const recordWithABSR = await extractABSRFromResults(
        topSellerRankFilltered,
        market,
        marketUrl
      );

      const numberKeys = [
        "topSellerRank",
        "price",
        "numReviews",
        "rating",
        "absr",
      ];
      let fullRecords = JSON.parse(
        JSON.stringify([...recordWithABSR, ...otherRecords])
      );
      fullRecords = fullRecords
        .map((x) => {
          if (x["url"]) {
            let cleanedUrl = amazonHelper.cleanAmazonUrl(
              `${marketLinks[market]}${x.url}`
            );
            x["url"] = cleanedUrl;
          }
          return formatAndCleanNumber(x, numberKeys);
        })
        .sort(function (a, b) {
          return a.topSellerRank - b.topSellerRank;
        });

      const formatedResponse = formatFunc(recordWithABSR, url);

      return {
        historic: fullRecords,
        category: formatedResponse,
        catId: id,
      };
    } else {
      console.log(`Failed to get html`);
      utilService.sendAndLogGeneralInformation(`Failed to get html for ${url}`);
      return null;
    }
  } catch (error) {
    console.log(
      `Something went wrong while scraping ${url} --> ERROR --> ${error.message}`
    );
    utilService.sendAndLogGeneralInformation(
      `Something went wrong while scraping ${url} --> ERROR --> ${error.message}`
    );
    return null;
  }
}

module.exports = {
  scrape,
  helper: amazonHelper
};