const got = require("got");
const { logger } = require("../logger/loggerService");

const { SLACK_WEB_HOOK } = process.env;

const sendScrapedMarketInformationNotifation = async ({
  text,
  type = "warning",
  market,
  category,
  count,
}) => {
  const slackMessagePayload = {
    username: "Error notifier",
    text: text || "Rocket Category Scraper Status",
    icon_emoji: ":skull:",
    attachments: [
      {
        color: type == "warning" ? "#eed140" : "#f44336",
        fields: [
          {
            title: "Market",
            value: market,
            short: true,
          },
          {
            title: "Category",
            value: category,
            short: true,
          },
          {
            title: "Count",
            value: count,
            short: true,
          },
        ],
      },
    ],
  };

  const { body } = await got.post(SLACK_WEB_HOOK, {
    json: slackMessagePayload,
    // responseType: "json",
  });

  logger.info("Slack notification sent");
};

const generalInformationNotifation = async ({ message }) => {
  const slackMessagePayload = {
    username: "Error notifier",
    text: "Rocket Category Scraper Info",
    icon_emoji: ":skull:",
    attachments: [
      {
        color: "#248ec2",
        fields: [
          {
            title: "Message",
            value: message,
            short: true,
          },
        ],
      },
    ],
  };

  const { body } = await got.post(SLACK_WEB_HOOK, {
    json: slackMessagePayload,
    // responseType: "json",
  });
};

const notfify = {
  sendScrapedMarketInformationNotifation,
  generalInformationNotifation,
};

module.exports.notification = notfify;
