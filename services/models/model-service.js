const Sequelize = require("sequelize")
const serviceHelper = require("../../helpers/service.helper")

class ModelService {

    model = null
    columnList = []
    constructor(model) {
        this.model = model
        model.describe().then((schema) => {
            this.columnList = Object.keys(schema)
            // console.log(`This is the colmu ->`, this.columnList);

        })
    }

    async getAll(options, isPaginate = false) {
        try {
            //TODO: Handle Pagination
            let data;
            const queryOptions = { order: [['id', 'DESC']], ...options }
            if (isPaginate) {
                const { docs, pages, total } = await this.model.paginate(queryOptions)
                data = { data: docs, pages, total }
            } else {
                data = await this.model.findAll(queryOptions)
            }
            return serviceHelper.successService(data)
        } catch (error) {
            console.log(`Error in getAll ->`, error.message);

            return serviceHelper.failedService()
        }

    }


    async getById(id, options = {}) {
        try {
            const data = await this.model.findOne({ where: { id }, order: [['id', 'DESC']], ...options })
            return serviceHelper.successService(data)
        } catch (error) {
            console.log(error.message);

            return serviceHelper.failedService()
        }
    }

    async findOneWhere(clause, options = {}) {
        try {
            const data = await this.model.findOne({ where: clause, order: [['id', 'DESC']], ...options })
            return serviceHelper.successService(data)
        } catch (error) {
            console.log(error);

            return serviceHelper.failedService(error, error.message)
        }
    }


    async findAllWhere(clause, options = {}, isPaginate = false) {
        try {

            const queryOptions = { where: clause, order: [['id', 'DESC']], ...options }
            // console.log(`queryOptions->`, queryOptions);
            // const data = await this.model.findAll({ where: clause, ...options })
            let data;
            if (isPaginate) {
                const { docs, pages, total } = await this.model.paginate(queryOptions)
                data = { data: docs, pages, total }
            } else {
                data = await this.model.findAll(queryOptions)
            }
            return serviceHelper.successService(data)
        } catch (error) {
            console.log(error);
            return serviceHelper.failedService(error, error.message)
        }
    }

    async getByAttributeLike(attribute, likeQuery, options = {}, isPaginate = false) {
        const { substring } = Sequelize.Op
        const clause = {
            [attribute]: {
                [substring]: `${likeQuery}`
            }
        }
        return this.findAllWhere(clause, options, true)
    }

    async create(data) {

        try {
            const modelInstance = await this.model.create(data)
            return serviceHelper.successService(modelInstance)
        } catch (error) {
            console.log(error.message);
            console.log(error);

            return serviceHelper.failedService()
        }
    }


    async update(id, data) {
        try {
            const updateInstance = await this.model.update(data, {
                where: {
                    id: id
                }
            })
            return serviceHelper.successService(updateInstance)
        } catch (error) {
            console.log(error.message);
            return serviceHelper.failedService()
        }
    }

    async delete(id) {
        try {
            const deleteInstance = await this.model.destroy({
                where: {
                    id: id
                }
            })
            return serviceHelper.successService(deleteInstance)
        } catch (error) {
            console.log(error.message);
            return serviceHelper.failedService()
        }
    }


    async bulkAddOrUpdate(data, updateOnDuplicate = null) {

        try {
            const bulkCreateOptions = { returning: true }

            if (updateOnDuplicate != null) {
                bulkCreateOptions['updateOnDuplicate'] = updateOnDuplicate
            }
            const bulk = await this.model.bulkCreate(data, bulkCreateOptions)
            return serviceHelper.successService(bulk)
        } catch (error) {
            console.log(error);
            return serviceHelper.failedService()
        }

    }

}



module.exports = ModelService